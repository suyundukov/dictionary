package ru.iteco.training.suyundukov;

public class GettingSessionFactoryException extends RuntimeException {
    public GettingSessionFactoryException(String errorMessage){
        super(errorMessage);
    }
}
