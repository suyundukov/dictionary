package ru.iteco.training.suyundukov;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.List;
import java.util.Properties;

public class HibernateSessionFactory {

    private SessionFactory sessionFactory;
    private final Configuration configuration = new Configuration();

    public HibernateSessionFactory(Properties properties) {
        configuration.setProperties(properties);
    }

    public HibernateSessionFactory(Properties properties, List<Class> classes) {
        this(properties);
        classes.forEach(c -> configuration.addAnnotatedClass(c));
    }

    public SessionFactory getSessionFactory() {
        if (sessionFactory == null || sessionFactory.isClosed()) {
            sessionFactory = getNewSessionFactory();
        }
        return sessionFactory;
    }

    public SessionFactory getNewSessionFactory() {
        try {
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            if (sessionFactory == null){
                throw new GettingSessionFactoryException("sessionFactory == null");
            }
            return sessionFactory;
        } catch (Exception e) {
            throw new GettingSessionFactoryException(e.getMessage());
        }
    }

    public void shutdown(){
        getSessionFactory().close();
    }

    public Configuration getConfiguration() {
        return configuration;
    }
}
