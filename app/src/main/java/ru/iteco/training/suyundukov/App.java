package ru.iteco.training.suyundukov;

import com.google.common.base.Joiner;
import ru.iteco.training.suyundukov.sort.*;
import ru.iteco.training.suyundukov.sort.pivotStrategies.PivotStrategyType;
import ru.iteco.training.suyundukov.sort.sortStrategies.SortStrategyType;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Simple program for sorting arguments in natural order
 */
public class App {
    public static void main(String[] args) {
        List<String> sequence = Arrays.asList(args);

        Sorter<String> sorter = SortBuilder.newBuilder()
                .PivotStrategy(PivotStrategyType.MIDDLE_ELEMENT)
                .SortStrategy(SortStrategyType.QUICK)
                .build(new Comparator<String>() {
                    public int compare(String o1, String o2) {
                        int x = Integer.parseInt(o1);
                        int y = Integer.parseInt(o2);
                        return x - y;
                    }
                });
        sorter.doSort(sequence);
        System.out.println(Joiner.on(" ").join(sequence));
    }
}
