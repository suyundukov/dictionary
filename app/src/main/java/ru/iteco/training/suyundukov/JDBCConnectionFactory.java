package ru.iteco.training.suyundukov;

import org.postgresql.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JDBCConnectionFactory {

    private String url;
    private Properties properties;

    public JDBCConnectionFactory(String url, Properties properties) {
        this.url = url;
        this.properties = properties;
    }

    public Connection getConnection() {
        try {
            DriverManager.registerDriver(new Driver());
            return DriverManager.getConnection(url, properties);
        } catch (SQLException ex) {
            throw new RuntimeException("Error connecting to the database", ex);
        }
    }
}
