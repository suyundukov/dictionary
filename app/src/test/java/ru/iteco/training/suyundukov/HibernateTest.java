package ru.iteco.training.suyundukov;

import org.hibernate.*;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.lock.OptimisticEntityLockException;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.*;

public class HibernateTest {

    private static Properties setting = new Properties();
    private static List<Class> annotatedClasses = new ArrayList<>();
    private static HibernateSessionFactory hibernateSessionFactory;

    private final static String DB_DRIVER = "org.postgresql.Driver";
    private final static String TEST_DB_URL = "jdbc:postgresql://localhost:5432/dictionary_test";
    private final static String USERNAME = "postgres";
    private final static String PASSWORD = "123";
    private final static String HBM2DLL_VALIDATE_MODE = "validate";
    private final static String CACHE_PROVIDER = "org.ehcache.jsr107.EhcacheCachingProvider";
    private final static String CACHE_REGION_FACTORY = "org.hibernate.cache.jcache.JCacheRegionFactory";

    @BeforeClass
    public static void setUp() {
        setting.put(Environment.DRIVER, DB_DRIVER);
        setting.put(Environment.URL, TEST_DB_URL);
        setting.put(Environment.USER, USERNAME);
        setting.put(Environment.PASS, PASSWORD);
        setting.put(Environment.HBM2DDL_AUTO, HBM2DLL_VALIDATE_MODE);
        setting.put("hibernate.javax.cache.provider", CACHE_PROVIDER);
        setting.put(Environment.CACHE_REGION_FACTORY, CACHE_REGION_FACTORY);
        annotatedClasses.add(Country.class);
        annotatedClasses.add(Region.class);
        annotatedClasses.add(City.class);
        annotatedClasses.add(Mayor.class);
        annotatedClasses.add(Attribute.class);
        annotatedClasses.add(AttributeType.class);
        hibernateSessionFactory = new HibernateSessionFactory(setting, annotatedClasses);
    }

    @Before
    public void clearAllData() {
        List<AbstractDAO> daoList = new ArrayList<>();
        daoList.add(new CountryDAO(hibernateSessionFactory.getSessionFactory()));
        daoList.add(new RegionDAO(hibernateSessionFactory.getSessionFactory()));
        daoList.add(new CityDAO(hibernateSessionFactory.getSessionFactory()));
        daoList.add(new AttributeDAO(hibernateSessionFactory.getSessionFactory()));
        daoList.add(new AttributeTypeDAO(hibernateSessionFactory.getSessionFactory()));
        daoList.add(new MayorDAO(hibernateSessionFactory.getSessionFactory()));
        daoList.forEach(d -> d.deleteAll());
    }

    @Test
    public void testSaveCountry() {
        CountryDAO countryDAO = new CountryDAO(hibernateSessionFactory.getSessionFactory());
        Country country = new Country("Country");
        countryDAO.save(country);
        List<Country> countries = countryDAO.getList();
        Country countryFromTable = countryDAO.getByName("Country");
        assertEquals(country.getId(), countryFromTable.getId());
        assertEquals("Country", countries.get(0).getName());
    }

    @Test
    public void testCacheCountry() {
        CountryDAO countryDAO = new CountryDAO(hibernateSessionFactory.getSessionFactory());
        Country country = new Country("Country");
        countryDAO.save(country);
        Long id = country.getId();
        hibernateSessionFactory.shutdown();
        hibernateSessionFactory.getConfiguration().setProperty(Environment.GENERATE_STATISTICS, "true");
        Transaction transaction = null;
        try (Session session = hibernateSessionFactory.getNewSessionFactory().openSession()) {
            session.beginTransaction();
            //fetch the country entity from database first time
            country = (Country) session.load(Country.class, id);
            assertEquals("Country", country.getName());
            //fetch the country entity again; Fetched from first level cache
            country = (Country) session.load(Country.class, id);
            assertEquals("Country", country.getName());
            session.getTransaction().commit();
        } catch (Exception ex) {
            if (transaction.getStatus() == TransactionStatus.ACTIVE
                    || transaction.getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                transaction.rollback();
            }
            throw ex;
        }
        //Try to get country in new session
        try (Session session = hibernateSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            //Here entity is already in second level cache so no database query will be hit
            country = (Country) session.load(Country.class, id);
            assertEquals("Country", country.getName());
            session.getTransaction().commit();
        } catch (Exception ex) {
            if (transaction.getStatus() == TransactionStatus.ACTIVE
                    || transaction.getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                transaction.rollback();
            }
            throw ex;
        }
        assertEquals(1, hibernateSessionFactory.getSessionFactory().getStatistics().getEntityFetchCount());
        assertEquals(1, hibernateSessionFactory.getSessionFactory().getStatistics().getSecondLevelCacheHitCount());
        hibernateSessionFactory.shutdown();
    }

    @Test
    public void testSaveCountryWithRegion() {
        CountryDAO countryDAO = new CountryDAO(hibernateSessionFactory.getSessionFactory());
        RegionDAO regionDAO = new RegionDAO(hibernateSessionFactory.getSessionFactory());
        Country country = new Country("Country");
        Region region = new Region("Region");
        City city = new City("City");

        region.getCities().add(city);
        city.setRegion(region);
        country.getRegions().add(region);
        region.setCountry(country);

        countryDAO.save(country);

        List<Region> regions = regionDAO.getList();
        assertEquals("Region", regions.get(0).getName());
        assertEquals(region.getId(), regionDAO.getByName("Region").getId());
        assertEquals(country.getId(), regions.get(0).getCountry().getId());
    }

    @Test
    public void testCityAttributeDAO() {
        CountryDAO countryDAO = new CountryDAO(hibernateSessionFactory.getSessionFactory());
        AttributeTypeDAO attributeTypeDAO = new AttributeTypeDAO(hibernateSessionFactory.getSessionFactory());
        AttributeDAO attributeDAO = new AttributeDAO(hibernateSessionFactory.getSessionFactory());
        Country country = new Country("Country");
        Region region = new Region("Region");
        City city = new City("City");
        Attribute attribute = new Attribute("Name", "Value");
        AttributeType attributeType = new AttributeType("Attribute Type");

        country.getRegions().add(region);
        region.setCountry(country);
        region.getCities().add(city);
        city.setRegion(region);
        city.getAttributes().add(attribute);
        attribute.setAttributeType(attributeType);
        attributeType.getAttributes().add(attribute);

        attributeTypeDAO.save(attributeType);
        countryDAO.save(country);
        assertEquals(attributeType.getId(), attributeTypeDAO.getByName("Attribute Type").getId());
        assertEquals(attribute.getId(), attributeDAO.getByName("Name").getId());

    }

    @Test
    public void testCityMayorDAO() {
        CountryDAO countryDAO = new CountryDAO(hibernateSessionFactory.getSessionFactory());
        MayorDAO mayorDAO = new MayorDAO(hibernateSessionFactory.getSessionFactory());
        Country country = new Country("Country");
        Region region = new Region("Region");
        City city = new City("City");
        Mayor mayor = new Mayor("Mayor");

        country.getRegions().add(region);
        region.setCountry(country);
        region.getCities().add(city);
        city.setRegion(region);
        city.setMayor(mayor);

        countryDAO.save(country);
        assertEquals(mayor.getId(), mayorDAO.getByName("Mayor").getId());
    }

    @Test(expected = OptimisticEntityLockException.class)
    public void tesCountryOptimisticLock() {
        //Saving Entity Country. id = 0; name = Country; version = 0
        CountryDAO countryDAO = new CountryDAO(hibernateSessionFactory.getSessionFactory());
        Country country = new Country("Country");
        countryDAO.save(country);
        long id = country.getId();
        SessionFactory sessionFactory = hibernateSessionFactory.getSessionFactory();
        Transaction transaction = null;
        //Getting saved Country
        Country futureCountry = countryDAO.getById(country.getId());
        try (Session session = sessionFactory.openSession()) {
            //Updating Country. Set new name = New Country
            country.setName("New Country");
            //Optimistic Locking country
            session.buildLockRequest(new LockOptions(LockMode.OPTIMISTIC))
                    .lock(country);
            //Begin transaction
            transaction = session.beginTransaction();
            session.update(country);
            session.flush();
            // Parallel updating Country in DB. name = Future Country; version = 1
            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                futureCountry.setName("Future Country");
                countryDAO.update(futureCountry);
            });
            future.join();
            assertEquals("Future Country", countryDAO.getById(id).getName());
            //country.version = 0; futureCountry.version = 1
            transaction.commit();
        } catch (Exception ex) {
            if (transaction.getStatus() == TransactionStatus.ACTIVE
                    || transaction.getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                transaction.rollback();
            }
            throw ex;
        }
    }

    @Test
    public void testCountryPessimisticLock() throws InterruptedException {
        // Saving Entity Country. id = 0; name = Country
        CountryDAO countryDAO = new CountryDAO(hibernateSessionFactory.getSessionFactory());
        Country country = new Country("Country");
        countryDAO.save(country);
        long id = country.getId();
        SessionFactory sessionFactory = hibernateSessionFactory.getSessionFactory();
        Transaction transaction = null;
        // Getting saved Country
        Country futureCountry = countryDAO.getById(country.getId());
        try (Session session = sessionFactory.openSession()) {
            // Updating Country. Set new name = New Country
            country.setName("New Country");
            //Pessimistic Locking country on read
            session.buildLockRequest(new LockOptions(LockMode.PESSIMISTIC_READ))
                    .lock(country);
            // Begin transaction
            transaction = session.beginTransaction();
            session.update(country);
            session.flush();
            // Parallel trying to update Country in DB. name = Future Country
            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                futureCountry.setName("Future Country");
                countryDAO.update(futureCountry);
            });
            Thread.sleep(3000);
            // Country Entity Locked
            assertEquals("Country", countryDAO.getById(id).getName());
            assertFalse(future.isDone());
            transaction.commit();
            // After commit Country is not locked. future can be done
            future.join();
            assertEquals("Future Country", countryDAO.getById(id).getName());
            assertTrue(future.isDone());
        } catch (Exception ex) {
            if (transaction.getStatus() == TransactionStatus.ACTIVE
                    || transaction.getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                transaction.rollback();
            }
            throw ex;
        }
    }
}
