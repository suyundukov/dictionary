package ru.iteco.training.suyundukov;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PostgresJDBCTest {

    private Connection connection = null;
    private final Properties properties = new Properties();

    private final String TEST_DB_URL = "jdbc:postgresql://localhost:5432/dictionary_test";
    private final String USERNAME = "postgres";
    private final String PASSWORD = "123";

    @Before
    public void setUp() {
        properties.put("user", USERNAME);
        properties.put("password", PASSWORD);
        connection = new JDBCConnectionFactory(TEST_DB_URL, properties).getConnection();
    }

    @Test
    public void testCountrySave() {
        CountryDAOJdbc countryDAO = new CountryDAOJdbc(connection);
        countryDAO.deleteAll();
        Country country = new Country("Country");
        countryDAO.save(country);
        Country country1 = countryDAO.getById(country.getId());
        assertEquals("Country", country1.getName());
    }

    @Test
    public void testCountryDelete() {
        CountryDAOJdbc countryDAO = new CountryDAOJdbc(connection);
        countryDAO.deleteAll();
        Country country = new Country("Country");
        countryDAO.save(country);
        countryDAO.delete(country);
        List<Country> countries = countryDAO.getList();
        assertTrue(countries.isEmpty());
    }

    @Test
    public void testCountryGetList() {
        CountryDAOJdbc countryDAO = new CountryDAOJdbc(connection);
        countryDAO.deleteAll();
        Country country = new Country("Country 1");
        countryDAO.save(country);
        country = new Country("Country 2");
        countryDAO.save(country);
        List<Country> countries = countryDAO.getList();
        assertEquals("Country 1", countries.get(0).getName());
        assertEquals("Country 2", countries.get(1).getName());
    }

    @Test
    public void testCountryUpdate() {
        CountryDAOJdbc countryDAO = new CountryDAOJdbc(connection);
        countryDAO.deleteAll();
        Country country = new Country("Country 1");
        countryDAO.save(country);
        country.setName("Updated Country");
        countryDAO.update(country);
        assertEquals("Updated Country", countryDAO.getById(country.getId()).getName());
    }

    @After
    public void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
