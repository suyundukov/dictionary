package ru.iteco.training.suyundukov;

import java.sql.SQLException;

/**
 * Класс исключений, возникающих во время некорректной подготовки объекта PreparedStatement.
 */
public class PrepareStatementException extends SQLException {
    public PrepareStatementException(String message){
        super(message);
    }
}
