package ru.iteco.training.suyundukov;

import java.sql.Connection;

public class CountryDAOJdbc extends AbstractDAOJdbc<Country, Long> {

    public CountryDAOJdbc(Connection connection) {
        super(connection, new CountryMapper());
    }

    @Override
    protected String getTableName() {
        return "country";
    }

    @Override
    public Country getById(Long id) {
        String query = "SELECT * FROM country WHERE country_id = ?";
        return getBy(query, id).get(0);
    }

    @Override
    public void save(Country entity) {
        String query = "INSERT INTO country VALUES (DEFAULT, ?, ?)";
        super.save(query, entity);
    }

    @Override
    public void update(Country entity) {
        String query = String.format("UPDATE country SET country_name = ?, version = ? WHERE country_id = %s", entity.getId());
        super.update(query, entity);
    }

    @Override
    public void delete(Country entity) {
        String query = "DELETE FROM country WHERE country_id = ?";
        super.delete(query , entity);
    }

    @Override
    public Country getByName(String name) {
        String query = "SELECT * FROM country WHERE country_name = ?";
        return getBy(query, name).get(0);
    }

}
