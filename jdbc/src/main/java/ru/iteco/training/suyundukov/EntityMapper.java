package ru.iteco.training.suyundukov;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Интерфейс маппера сущностей.
 * @param <T> - Тип сущности
 */
public interface EntityMapper<T extends BaseEntity> {

    /**
     * Метод преобразующий полученный resultSet в набор сущностей.
     * @param rs - результирующий набор запроса
     * @return - набор сущностей
     * @throws SQLException
     */
    public List<T> map(ResultSet rs) throws SQLException;

    /**
     * Метод подгатавливает ps заполняя параметры значениями и сущности.
     * @param ps - подготовленное состояние, должно содержать все поля сущности за исключение идентификатора
     * @param entity - сущность для заполнения ps
     * @throws SQLException
     */
    public void prepare(PreparedStatement ps, T entity) throws SQLException;
}
