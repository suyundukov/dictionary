package ru.iteco.training.suyundukov;

import java.io.Serializable;
import java.sql.*;
import java.util.List;

public abstract class AbstractDAOJdbc<T extends BaseEntity, I extends Serializable> implements DAO<T, I> {

    private Connection connection;
    private EntityMapper<T> mapper;

    public AbstractDAOJdbc(Connection connection, EntityMapper mapper) {
        this.mapper = mapper;
        this.connection = connection;
    }

    protected abstract String getTableName();

    protected void save(String query, T entity) {
        try {
            JdbcHelper helper = new JdbcHelper(connection);
            entity.setId((I) helper.executeUpdate(query, entity, mapper));
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void update(String query, T entity) {
        try {
            JdbcHelper helper = new JdbcHelper(connection);
            helper.executeUpdate(query, entity, mapper);
        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    public void delete(String query, T entity) {
        try {
            JdbcHelper helper = new JdbcHelper(connection);
            helper.executeUpdate(query, entity.getId());
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void deleteAll() {
        String query = String.format("TRUNCATE TABLE %s CASCADE", getTableName());
        try {
            JdbcHelper helper = new JdbcHelper(connection);
            helper.executeUpdate(query);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public List<T> getList() {
        String query = String.format("SELECT * FROM %s", getTableName());
        return getBy(query);
    }

    protected List<T> getBy(String query, Object... params) {
        try {
            JdbcHelper helper = new JdbcHelper(connection);
            return (List<T>) helper.executeQuery(query, mapper, params);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

}
