package ru.iteco.training.suyundukov;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс помощник при выполнении sql запросов.
 * @param <I> - тип идентификатора (требуется для получения ИД при добавлении нового элемента)
 */
public class JdbcHelper<I extends Serializable> {

    private Connection connection = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;

    public JdbcHelper(Connection connection) {
        this.connection = connection;
    }

    /**
     * Метод выполняет запрос с помощью preparedStatement.executeUdate.
     * @param query - sql запрос
     * @param entity - сущность, для которой выполняется запрос
     * @param mapper - маппер, выполняющий подготовку состояния из сущности
     * @return - I - сгенерированный идентификатор сущности
     * @throws SQLException
     */
    public I executeUpdate(String query, BaseEntity entity, EntityMapper mapper) throws SQLException {
        try {
            ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            mapper.prepare(ps, entity);
            if (ps.executeUpdate() != 1) {
                throw new SQLException("Trying to modify more than 1 record");
            }
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                return (I) rs.getObject(1);
            }
            return null;
        } catch (SQLException ex) {
            throw new SQLException(ex);
        } finally {
            closeStatement();
        }
    }

    /**
     * Метод выполняет запрос с помощью preparedStatement.executeUdate.
     * @param query - sql запрос
     * @param params - набор параметров запроса, которые передаются в preparedStatement
     * @throws SQLException
     */
    public void executeUpdate(String query, Object... params) throws SQLException {
        try {
            ps = connection.prepareStatement(query);
            if (ps.getParameterMetaData().getParameterCount() != params.length) {
                throw new PrepareStatementException("The number of parameters is not equal to the number of values");
            }
            for (int i = 0; i < params.length; i++) {
                ps.setObject(i + 1, params[i]);
            }
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        } finally {
            closeStatement();
        }
    }

    /**
     * Метод выполняет запрос с помощью preparedStatement.executeQuery.
     * @param query - sql запрос
     * @param mapper - маппер, преобразующий результирующий набор запрос в сущность
     * @param params - набор параметров запроса, которые передаются в preparedStatement
     * @return - набор сущностей
     * @throws SQLException
     */
    public List<? extends BaseEntity> executeQuery(String query, EntityMapper mapper, Object... params) throws SQLException {
        List<BaseEntity> list = new ArrayList<>();
        try {
            ps = connection.prepareStatement(query);
            if (ps.getParameterMetaData().getParameterCount() != params.length) {
                throw new PrepareStatementException("The number of parameters is not equal to the number of values");
            }
            for (int i = 0; i < params.length; i++) {
                ps.setObject(i + 1, params[i]);
            }
            rs = ps.executeQuery();
            return mapper.map(rs);
        } catch (SQLException ex) {
            throw new SQLException(ex);
        } finally {
            closeStatement();
        }
    }

    /**
     * Метод закрывает resultSet и preparedStatement.
     * Используется в блоке finally метод выполняющих работу resultSet и preparedStatement.
     */
    private void closeStatement() {
        if (rs != null) try {
            rs.close();
        } catch (SQLException ex) {
        }
        if (ps != null) try {
            ps.close();
        } catch (SQLException ex) {
        }
    }

}
