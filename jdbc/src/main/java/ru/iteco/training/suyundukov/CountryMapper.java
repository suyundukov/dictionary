package ru.iteco.training.suyundukov;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CountryMapper implements EntityMapper<Country> {

    @Override
    public List<Country> map(ResultSet rs) throws SQLException {
        List<Country> entities = new ArrayList<>();
        while (rs.next()) {
            Country entity = new Country();
            entity.setId(rs.getLong("country_id"));
            entity.setName(rs.getString("country_name"));
            entity.setVersion(rs.getInt("version"));
            entities.add(entity);
        }
        return entities;
    }

    @Override
    public void prepare(PreparedStatement ps, Country country) throws SQLException {
        int i = 1;
        if (country.getName() != null) ps.setString(i++, country.getName());
        if (country.getVersion() != null) ps.setInt(i++, country.getVersion());
        else  ps.setInt(i++, 0);
    }

}
