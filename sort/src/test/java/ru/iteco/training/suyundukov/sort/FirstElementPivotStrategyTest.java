package ru.iteco.training.suyundukov.sort;

import org.junit.Test;
import ru.iteco.training.suyundukov.sort.pivotStrategies.FirstElementPivotStrategy;
import ru.iteco.training.suyundukov.sort.sortStrategies.SortStrategyType;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class FirstElementPivotStrategyTest {

    @Test
    public void testFirstElementPivot() {

        FirstElementPivotStrategy firstElementPivotTest = new FirstElementPivotStrategy();
        int startIndex = 1;
        int endIndex = 5;
        int pivot = firstElementPivotTest.getPivot(startIndex, endIndex);
        assertEquals(startIndex, pivot);
    }
}
