package ru.iteco.training.suyundukov.sort;

import org.junit.Test;
import ru.iteco.training.suyundukov.sort.sortStrategies.SortStrategyType;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ImmutableSorterTest {

    @Test
    public void testBuildImmutable() {
        List<String> sequence = Arrays.asList(new String[]{"3", "1", "2"});
        List<String> result = Arrays.asList(new String[]{"1", "2", "3"});
        Sorter<String> immutableSorter = SortBuilder.newBuilder()
                .SortStrategy(SortStrategyType.BUBBLE)
                .buildImmutable(new Comparator<String>() {
                    public int compare(String o1, String o2) {
                        int x = Integer.parseInt(o1);
                        int y = Integer.parseInt(o2);
                        return x - y;
                    }
                });

        List<String> immutableSorterResult = immutableSorter.doSort(sequence);
        assertNotEquals(sequence, immutableSorterResult);
        assertEquals(immutableSorterResult, result);
    }
}
