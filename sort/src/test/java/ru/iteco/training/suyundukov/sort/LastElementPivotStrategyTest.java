package ru.iteco.training.suyundukov.sort;

import org.junit.Test;
import ru.iteco.training.suyundukov.sort.pivotStrategies.LastElementPivotStrategy;

import static org.junit.Assert.assertEquals;

public class LastElementPivotStrategyTest {

    @Test
    public void testLastElementPivot() {

        LastElementPivotStrategy lastElementPivotTest = new LastElementPivotStrategy();
        int startIndex = 1;
        int endIndex = 5;
        int pivot = lastElementPivotTest.getPivot(startIndex, endIndex);
        assertEquals(endIndex, pivot);
    }
}
