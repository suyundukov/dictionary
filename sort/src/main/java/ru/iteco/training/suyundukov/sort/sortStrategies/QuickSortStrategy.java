package ru.iteco.training.suyundukov.sort.sortStrategies;

import ru.iteco.training.suyundukov.sort.pivotStrategies.PivotStrategy;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Реализация стратегии быстрой сортировки
 */
public class QuickSortStrategy<T> implements SortStrategy<T> {

    PivotStrategy pivotStrategy;
    Comparator<T> comparator;

    public QuickSortStrategy(PivotStrategy pivotStrategy) {
        this.pivotStrategy = pivotStrategy;
    }

    /**
     * Метод сортировки
     * @param array - исходный массив элементов
     * @param comparator - компаратор
     */
    @Override
    public void sort(List<T> array, Comparator<T> comparator) {
        this.comparator = comparator;
        quickSort(array, 0, array.size() - 1);
    }

    /**
     * Метод быстрой сортировки
     * @param array - исходный массив элементов
     * @param startIndex - левая граница массива
     * @param endIndex - правая граница массива
     */
    private void quickSort(List<T> array, Integer startIndex, Integer endIndex) {
        if (startIndex >= endIndex)
            return;

        Integer pivot = pivotStrategy.getPivot(startIndex, endIndex);
        T supportElement = array.get(pivot);

        Integer i = startIndex;
        Integer j = endIndex;

        while (i <= j) {
            while (i < pivot && (comparator.compare(array.get(i), supportElement) <= 0)) {
                i++;
            }
            while (j > pivot && (comparator.compare(supportElement, array.get(j)) <= 0)) {
                j--;
            }
            if (i <= j) {
                Collections.swap(array, i, j);
                i++;
                j--;
            }
        }
        if (startIndex < j)
            quickSort(array, startIndex, j);
        if (endIndex > i)
            quickSort(array, i, endIndex);
    }
}
