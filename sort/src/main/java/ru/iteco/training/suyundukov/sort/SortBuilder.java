package ru.iteco.training.suyundukov.sort;

import ru.iteco.training.suyundukov.sort.pivotStrategies.PivotStrategyType;
import ru.iteco.training.suyundukov.sort.sortStrategies.SortStrategyType;

import java.util.Comparator;

/**
 * Класс-конструктор сортировщика массива
 */
public class SortBuilder {

    private SortStrategyType sortStrategyType;
    private PivotStrategyType pivotStrategyType;

    private SortBuilder() {
    }

    public static SortBuilder newBuilder() {
        return new SortBuilder();
    }

    public SortBuilder SortStrategy(SortStrategyType type) {
        sortStrategyType = type;
        return this;
    }

    public SortBuilder PivotStrategy(PivotStrategyType type) {
        pivotStrategyType = type;
        return this;
    }

    public <T> Sorter<T> build(Comparator<T> comparator) {
        if (comparator == null){
            throw new IllegalArgumentException("Undefined comparator");
        }
        return new MutableSorter<>(comparator, SortStrategyFactory.<T>getSortStrategy(sortStrategyType, pivotStrategyType));
    }

    public <T> Sorter<T> buildImmutable(Comparator<T> comparator){
        if (comparator == null){
            throw new IllegalArgumentException("Undefined comparator");
        }
        return new ImmutableSorter<>(comparator, SortStrategyFactory.<T>getSortStrategy(sortStrategyType, pivotStrategyType));
    }
}
