package ru.iteco.training.suyundukov.sort.sortStrategies;

import java.util.Comparator;
import java.util.List;

/**
 * Реализация стратегии сортировки пузырьком
 */
public class BubbleSortStrategy<T> implements SortStrategy<T> {
    /**
     * Метод сортировки
     * @param array - исходный массив элементов
     * @param comparator - компаратор
     */
    @Override
    public void sort(List<T> array, Comparator<T> comparator) {
        for (int i = 0; i < array.size(); i++) {
            T current = array.get(i);

            for (int j = i - 1; j >= 0; j--) {
                T left = array.get(j);

                if (comparator.compare(current, left) < 0) {
                    array.set(j + 1, left);
                    array.set(j, current);
                } else {
                    break;
                }
            }
        }
    }
}
