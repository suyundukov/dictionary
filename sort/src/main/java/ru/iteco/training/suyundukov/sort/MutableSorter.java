package ru.iteco.training.suyundukov.sort;

import ru.iteco.training.suyundukov.sort.sortStrategies.SortStrategy;

import java.util.Comparator;

/**
 * Класс сортировщика, изменябщего исходную коллекцию
 */
public class MutableSorter<T> extends BaseSorter<T> {

    public MutableSorter(Comparator<T> comparator, SortStrategy<T> sortStrategy) {
        super(comparator, sortStrategy);
    }
}
