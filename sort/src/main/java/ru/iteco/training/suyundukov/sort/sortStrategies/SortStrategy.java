package ru.iteco.training.suyundukov.sort.sortStrategies;

import java.util.Comparator;
import java.util.List;

public interface SortStrategy<T> {
    public void sort(List<T> array, Comparator<T> comparator);
}
