package ru.iteco.training.suyundukov.sort;

import ru.iteco.training.suyundukov.sort.sortStrategies.SortStrategy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Класс сортировщика, не меняющего исходную коллекцию
 */
public class ImmutableSorter<T> extends BaseSorter<T> {

    public ImmutableSorter(Comparator<T> comparator, SortStrategy<T> sortStrategy) {
        super(comparator, sortStrategy);
    }

    public List<T> doSort(List<T> list) {
        List<T> listToSort = new ArrayList<>(list);
        return super.doSort(listToSort);
    }
}
