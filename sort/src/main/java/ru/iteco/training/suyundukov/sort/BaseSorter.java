package ru.iteco.training.suyundukov.sort;

import ru.iteco.training.suyundukov.sort.sortStrategies.SortStrategy;

import java.util.Comparator;
import java.util.List;

/**
 * Базовый класс сортировщика
 */
public class BaseSorter<T> implements Sorter<T> {

    private SortStrategy<T> sortStrategy;
    private Comparator<T> comparator;

    public BaseSorter(Comparator<T> comparator, SortStrategy<T> sortStrategy){
        this.comparator = comparator;
        this.sortStrategy = sortStrategy;
    }

    public List<T> doSort(List<T> array) {
        sortStrategy.sort(array, comparator);
        return array;
    }
}
