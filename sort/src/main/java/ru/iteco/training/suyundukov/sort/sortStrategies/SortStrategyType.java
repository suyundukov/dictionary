package ru.iteco.training.suyundukov.sort.sortStrategies;

public enum SortStrategyType {
    DEFAULT,
    BUBBLE,
    QUICK
}
