package ru.iteco.training.suyundukov.sort.pivotStrategies;

public class MiddleElementPivotStrategy implements PivotStrategy {
    public Integer getPivot(Integer StartIndex, Integer LastIndex) {
        return StartIndex + (LastIndex - StartIndex)/2;
    }
}
