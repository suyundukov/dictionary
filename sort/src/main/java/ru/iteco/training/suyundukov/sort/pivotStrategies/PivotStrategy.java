package ru.iteco.training.suyundukov.sort.pivotStrategies;

public interface PivotStrategy {
    public Integer getPivot(Integer StartIndex, Integer LastIndex);
}
