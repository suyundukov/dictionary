package ru.iteco.training.suyundukov.sort;

import ru.iteco.training.suyundukov.sort.pivotStrategies.*;
import ru.iteco.training.suyundukov.sort.sortStrategies.BubbleSortStrategy;
import ru.iteco.training.suyundukov.sort.sortStrategies.QuickSortStrategy;
import ru.iteco.training.suyundukov.sort.sortStrategies.SortStrategy;
import ru.iteco.training.suyundukov.sort.sortStrategies.SortStrategyType;

/**
 * Фабрика стратегий сортировки и выбора опорного элемента
 */
class SortStrategyFactory {

    static <T> SortStrategy<T> getSortStrategy(SortStrategyType strategyType, PivotStrategyType pivotType) {
        if (strategyType == null) strategyType = SortStrategyType.DEFAULT;

        SortStrategy<T> strategy;
        switch (strategyType) {
            case BUBBLE:
                strategy = new BubbleSortStrategy<T>();
                break;
            case DEFAULT:
            case QUICK:
                strategy = new QuickSortStrategy<T>(SortStrategyFactory.<T>pivot(pivotType));
                break;
            default:
                throw new RuntimeException("Unknown sort type");
        }
        return strategy;
    }

    private static PivotStrategy pivot(PivotStrategyType type) {
        if (type == null) type = PivotStrategyType.DEFAULT;

        PivotStrategy pivot;
        switch (type) {
            case FIRST_ELEMENT:
                pivot = new FirstElementPivotStrategy();
                break;
            case LAST_ELEMENT:
                pivot = new LastElementPivotStrategy();
                break;
            case MIDDLE_ELEMENT:
                pivot = new MiddleElementPivotStrategy();
                break;
            case DEFAULT:
            default:
                pivot = new MiddleElementPivotStrategy();
        }
        return pivot;
    }
}