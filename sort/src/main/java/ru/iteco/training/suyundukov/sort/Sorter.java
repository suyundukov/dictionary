package ru.iteco.training.suyundukov.sort;

import java.util.List;

public interface Sorter<T> {
    List<T> doSort(List<T> array);
}
