package ru.iteco.training.suyundukov.sort.pivotStrategies;

public enum PivotStrategyType {
    DEFAULT,
    FIRST_ELEMENT,
    LAST_ELEMENT,
    MIDDLE_ELEMENT
}
