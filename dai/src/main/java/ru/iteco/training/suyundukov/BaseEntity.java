package ru.iteco.training.suyundukov;

import java.io.Serializable;

public interface BaseEntity<I extends Serializable> {

    Long getId();

    void setId(I id);

    String getName();

    void setName(String name);
}
