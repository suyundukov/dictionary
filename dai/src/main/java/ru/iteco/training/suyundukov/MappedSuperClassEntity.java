package ru.iteco.training.suyundukov;

import javax.persistence.*;

@MappedSuperclass
public abstract class MappedSuperClassEntity implements BaseEntity<Long> {

    @Id
    private Long id;

    @Column
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
