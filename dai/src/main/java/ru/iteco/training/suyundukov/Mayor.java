package ru.iteco.training.suyundukov;

import javax.persistence.*;

@Entity(name = "mayor")
public class Mayor implements BaseEntity<Long>{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MAYOR_SEQ")
    @SequenceGenerator(name = "MAYOR_SEQ", sequenceName = "mayor_mayor_id_seq", allocationSize = 1)
    @Column(name = "mayor_id")
    private Long id;

    @Column(name = "mayor_fio")
    private String name;

    public Mayor(){}

    public Mayor(String fio){
        name = fio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
