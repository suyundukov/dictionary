package ru.iteco.training.suyundukov;

import java.io.Serializable;
import java.util.List;

/**
 * Интерфейс доступа к данным
 * @param <T> - Тип сущности
 * @param <I> - Тип идентификатора
 */
public interface DAO<T extends BaseEntity, I extends Serializable> {

    void save(T entity);

    void update(T entity);

    void delete(T entity);

    void deleteAll();

    List<T> getList();

    T getByName(String name);

    T getById(I id);
}
