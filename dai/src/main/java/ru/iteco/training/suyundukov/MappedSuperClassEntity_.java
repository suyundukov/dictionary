package ru.iteco.training.suyundukov;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(MappedSuperClassEntity.class)
public class MappedSuperClassEntity_ {
    public static volatile SingularAttribute<Country, Long> id;
    public static volatile SingularAttribute<Country, String> name;
}
