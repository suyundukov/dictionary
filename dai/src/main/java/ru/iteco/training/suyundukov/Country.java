package ru.iteco.training.suyundukov;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "country")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "country")
public class Country implements BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COUNTRY_SEQ")
    @SequenceGenerator(name = "COUNTRY_SEQ", sequenceName = "country_country_id_seq", allocationSize = 1)
    @Column(name = "country_id")
    private Long id;

    @Column(name = "country_name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "country", orphanRemoval = true)
    private List<Region> regions = new ArrayList<>();

    @Version
    @Column(name = "version")
    private Integer version;

    public Country() {
    }

    public Country(String name) {
        setName(name);
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
