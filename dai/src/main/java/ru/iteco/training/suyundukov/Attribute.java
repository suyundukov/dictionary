package ru.iteco.training.suyundukov;

import javax.persistence.*;

@Entity(name = "attribute")
public class Attribute implements BaseEntity<Long>{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ATTRIBUTE_SEQ")
    @SequenceGenerator(name = "ATTRIBUTE_SEQ", sequenceName = "attribute_attribute_id_seq", allocationSize = 1)
    @Column(name = "attribute_id")
    private Long id;

    @Column(name = "attribute_name")
    private String name;

    @Column(name = "attribute_value")
    private String value;

    @ManyToOne
    @JoinColumn(name = "attribute_type_id", nullable = false)
    private AttributeType attributeType;

    public Attribute() {
    }

    public Attribute(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public AttributeType getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(AttributeType attributeType) {
        this.attributeType = attributeType;
    }
}
