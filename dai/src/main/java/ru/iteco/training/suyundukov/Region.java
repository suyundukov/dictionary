package ru.iteco.training.suyundukov;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "region")
public class Region implements BaseEntity<Long>{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REGION_SEQ")
    @SequenceGenerator(name = "REGION_SEQ", sequenceName = "region_region_id_seq", allocationSize = 1)
    @Column(name = "region_id")
    private Long id;

    @Column(name = "region_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false)
    private Country country;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "region", orphanRemoval = true)
    private List<City> cities = new ArrayList<>();

    public Region() {
    }

    public Region(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
