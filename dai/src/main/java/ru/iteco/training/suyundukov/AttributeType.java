package ru.iteco.training.suyundukov;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "attribute_type")
public class AttributeType implements BaseEntity<Long>{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ATTRIBUTE_TYPE_SEQ")
    @SequenceGenerator(name = "ATTRIBUTE_TYPE_SEQ", sequenceName = "attribute_type_attribute_type_id_seq", allocationSize = 1)
    @Column(name = "attribute_type_id")
    private Long id;

    @Column(name = "attribute_type")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "attributeType", orphanRemoval = true)
    private List<Attribute> attributes = new ArrayList<>();

    public AttributeType() {
    }

    public AttributeType(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }
}
