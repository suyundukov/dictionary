package ru.iteco.training.suyundukov;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Country.class)
public class Country_ extends MappedSuperClassEntity_ {
    public static volatile ListAttribute<Country, Region> regions;
    public static volatile SingularAttribute<Country, Integer> version;
}
