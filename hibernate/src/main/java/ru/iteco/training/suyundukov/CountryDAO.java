package ru.iteco.training.suyundukov;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class CountryDAO extends AbstractDAO<Country, Long> {

    public CountryDAO(SessionFactory sessionFactory) {
        super(sessionFactory, Country.class);
    }

    /**
     * Проба реализации через метамодель
     * @param name
     * @return
     *//*
    @Override
    public Country getByName(String name) {
        try (Session session = super.sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery criteria = builder.createQuery(Country.class);
            Root<Country> root = criteria.from(Country.class);
            criteria.where(builder.equal(root.get(Country_.name), name));
            return (Country) session.createQuery(criteria).getSingleResult();
        } catch (Exception e) {
            throw new DAORuntimeException(e.getMessage());
        }
    }*/
}
