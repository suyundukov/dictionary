package ru.iteco.training.suyundukov;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.List;
import java.util.function.BiConsumer;

public abstract class AbstractDAO<T extends BaseEntity, I extends Serializable> implements DAO<T, I> {

    private Class<T> clazz;
    protected SessionFactory sessionFactory;

    public AbstractDAO(SessionFactory sessionFactory, Class<T> clazz) {
        this.sessionFactory = sessionFactory;
        this.clazz = clazz;
    }

    public void save(T instance) {
        transactionAction((session, x) -> session.save(x), instance);
    }

    public void update(T instance) {
        transactionAction((session, x) -> session.update(x), instance);
    }

    public void delete(T instance) {
        transactionAction((session, x) -> session.delete(x), instance);
    }

    public void deleteAll() {
        transactionAction((session, x) -> getList().forEach( c -> {session.delete(c);}), null);
    }

    private void transactionAction(BiConsumer<Session, T> biConsumer, T x) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            biConsumer.accept(session, x);
            session.flush();
            transaction.commit();
        } catch (Exception e) {
            if (transaction.getStatus() == TransactionStatus.ACTIVE
                    || transaction.getStatus() == TransactionStatus.MARKED_ROLLBACK) {
                transaction.rollback();
            }
            throw new DAORuntimeException(e.getMessage());
        }
    }

    public List<T> getList() {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery criteria = builder.createQuery(clazz);
            criteria.from(clazz);
            return session.createQuery(criteria).getResultList();
        } catch (Exception e) {
            throw new DAORuntimeException(e.getMessage());
        }
    }

    public T getByName(String name) {
        return getBy("name", name);
    }

    public T getById(I id) {
        return getBy("id", id);
    }

    private T getBy(String param, Object value){
        try (Session session = sessionFactory.openSession()) {
            String queryString = String.format("from %s where %s = :value ", clazz.getName(), param);
            Query query = session.createQuery(queryString);
            query.setParameter("value", value);
            return (T) query.getSingleResult();
        } catch (Exception e) {
            throw new DAORuntimeException(e.getMessage());
        }
    }
}
