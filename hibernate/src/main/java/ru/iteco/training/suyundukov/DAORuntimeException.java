package ru.iteco.training.suyundukov;

public class DAORuntimeException extends RuntimeException{
    public DAORuntimeException(String errorMessage){
        super(errorMessage);
    }
}
