package ru.iteco.training.suyundukov;

import org.hibernate.SessionFactory;

public class CityDAO extends AbstractDAO<City, Long> {

    public CityDAO(SessionFactory sessionFactory) {
        super(sessionFactory, City.class);
    }
}
