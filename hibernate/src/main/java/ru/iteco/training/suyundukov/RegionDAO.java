package ru.iteco.training.suyundukov;

import org.hibernate.SessionFactory;

public class RegionDAO extends AbstractDAO<Region, Long> {

    public RegionDAO(SessionFactory sessionFactory) {
        super(sessionFactory, Region.class);
    }
}
