package ru.iteco.training.suyundukov;

import org.hibernate.SessionFactory;

public class MayorDAO extends AbstractDAO<Mayor, Long> {
    public MayorDAO(SessionFactory sessionFactory) {
        super(sessionFactory, Mayor.class);
    }
}
