package ru.iteco.training.suyundukov;

import org.hibernate.SessionFactory;

public class AttributeDAO extends AbstractDAO<Attribute, Long> {

    public AttributeDAO(SessionFactory sessionFactory) {
        super(sessionFactory, Attribute.class);
    }
}
