package ru.iteco.training.suyundukov;

import org.hibernate.SessionFactory;

public class AttributeTypeDAO extends AbstractDAO<AttributeType, Long> {

    public AttributeTypeDAO(SessionFactory sessionFactory) {
        super(sessionFactory, AttributeType.class);
    }
}
